package main

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func Test_ResultIndex(t *testing.T) {
	req, err := http.NewRequest("GET", "http://example.com/result", nil)
	if err != nil {
		t.Fatal(err)
	}

	res := httptest.NewRecorder()
	ResultIndex(res, req)

	exp := "{\"pi\":0,\"terms\":0}"
	act := res.Body.String()
	if exp != act {
		t.Fatalf("Expected %s got %s", exp, act)
	}
}
