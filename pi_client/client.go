package piClient

import (
	"log"
	"time"

	pb "gitlab.com/andredurao/grpc-pi/pi"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

const (
	address = "localhost:50051"
)

func Consume(ch chan float64, n uint64) {
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	c := pb.NewTermCalculatorClient(conn)

	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
	defer cancel()

	r, err := c.Compute(ctx, &pb.TermRequest{Term: n})
	if err != nil {
		log.Fatalf("could not compute: %v", err)
	}
	ch <- r.Value
}
