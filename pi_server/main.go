package main

import (
	pb "gitlab.com/andredurao/grpc-pi/pi"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"log"
	"math"
	"net"
)

const (
	port = ":50051"
)

type server struct{}

func (s *server) Compute(ctx context.Context, in *pb.TermRequest) (*pb.TermReply, error) {
	numerator := 4.0 * (math.Pow(-1.0, (float64)(in.Term)))
	denominator := 2.0*(float64)(in.Term) + 1.0
	term := &pb.TermReply{Value: (numerator / denominator)}
	return term, nil
}

func main() {
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()
	pb.RegisterTermCalculatorServer(s, &server{})
	// Register reflection service on gRPC server.
	reflection.Register(s)
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
