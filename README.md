# Teste técnico Tracksale

## Tarefa
### Desenvolver um sistema em Go para cálculo distribuído do número irracional π (Pi).

## Descrição

O cálculo será feito utilizando qualquer uma das séries disponíveis na
literatura que aproximam Pi. Cada termo da série será obtido realizando uma requisição
concorrente ou paralela a um servidor, passando o termo da série desejado. O servidor
retornará o valor correspondente que deve ser somado ao acumulador. Note que quanto mais
termos utilizar, mais preciso será o número.
Comunicação entre a aplicação e o servidor deve ser feita em gRPC.

Criar também um endpoint REST para exibir o número já calculado e até qual termo da série
foi somado para obter aquele valor.

Criar o repositório no Git e compartilhar conosco. Fazer o maior número de commits
possível para acompanharmos o processo de dev.

Aguardamos sua resposta! Boa sorte!

## Proposta

### Instalando

* Com o comando go get, usando usuário e senha:

```
GIT_TERMINAL_PROMPT=1 go get -u gitlab.com/andredurao/grpc-pi
```

* Ao usar o comando `go get` se quiser instalar sem usar usuário / senha do gitlab (usando suas chaves ssh)

```
git config --global url."git@gitlab.com:".insteadOf "https://gitlab.com/"
go get -u gitlab.com/andredurao/grpc-pi
```

### gRPC
https://grpc.io/docs/tutorials/basic/go.html

### Implementar em gPRC o cálculo dos termos da série de Leibniz

https://pt.wikipedia.org/wiki/Fórmula_de_Leibniz_para_π

https://en.wikipedia.org/wiki/Pi#Rapidly_convergent_series
https://en.wikipedia.org/wiki/Pi#Infinite_series

### Protobuf

Gerando o contrato:

```bash
protoc -I pi/ pi/pi.proto --go_out=plugins=grpc:pi
```

### Executando

#### Via `go run`
* Em um terminal executar o servidor grpc: `$ go run pi_server/main.go`
* Em outro terminal executar o servidor app: `$ go run main.go`
* O resultado pode ser verificar usando curl, ou no browser: `$ curl localhost:8080/result`

#### Ou por `go install ./...` no diretório corrente
* Em um terminal executar o servidor grpc: `$ pi_server`
* Em outro terminal executar o servidor app: `$ grpc-pi`
* O resultado pode ser verificar usando curl, ou no browser: `$ curl localhost:8080/result`
