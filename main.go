package main

import (
	"encoding/json"
	"fmt"
	"gitlab.com/andredurao/grpc-pi/pi_client"
	"log"
	"net/http"
)

type Result struct {
	Pi    float64 `json:"pi"`
	Terms uint64  `json:"terms"`
}

const (
	address      = "localhost:50051"
	windowSize   = 4
	windowCounts = 10000
)

var result Result
var ch chan float64

func term(n uint64) {
	piClient.Consume(ch, n)
}

func enqueue() {
	for i := 0; i < windowCounts; i++ {
		for j := 0; j < windowSize; j++ {
			index := i*windowSize + j
			go term((uint64)(index))
		}
		for j := 0; j < windowSize; j++ {
			result.Terms++
			result.Pi += <-ch
		}
	}
}

func ResultIndex(rw http.ResponseWriter, r *http.Request) {
	str, _ := json.Marshal(result)
	rw.Header().Set("Content-Type", "application/json")
	fmt.Fprintf(rw, (string)(str))
}

func main() {
	ch = make(chan float64)
	result = Result{Pi: 0.0, Terms: 0}
	go enqueue()
	log.Println("Running at port 8080")
	http.HandleFunc("/result", ResultIndex)
	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
